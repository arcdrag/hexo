---
title: Typeclasses - Part II - Semigroup and Monoid
date: 2018-11-15 21:23:33
tags: 
- scala
- typeclass
- monoid
- semigroup
---

In the last post I talked about the Show typeclass.  Next we'll cover
Semigroup and Monoid.

While these have fancy sounding names, they are almost as simple as
Show.  Let's take a look at the interfaces.

``` scala
trait Semigroup[A] {
  def combine(x: A, y: A): A
}

trait Monoid[A] extends Semigroup[A] {
  def empty: A
}
```

A semigroup just says that you know how to combine something of this
type with another member of the same type, and the result will also be
of the same type.  Or in other terms, `A => A => A`.

A Monoid is a semigroup that also has an empty member.  So for an
Option, this would be None.  For a list, Nil.  For a String, "".

Let's go ahead and show why this pattern can be useful.

``` scala
implicit def AdditiveMonoid = new Monoid[Int] {
  override def empty = 0

  override def combine(x: Int, y: Int) = x + y
}

implicit def MapMonoid[K, V : Monoid] = new Monoid[Map[K, V]] {
  override def empty = Map.empty[K, V]

  override def combine(x: Map[K, V], y: Map[K, V]) = {
    val vM = implicitly[Monoid[V]]
    (x.keySet ++ y.keySet).toSeq.map { k =>
      val xv:   V = x.getOrElse(k, vM.empty)
      val yv:   V = y.getOrElse(k, vM.empty)
      val newV: V = vM.combine(xv, yv)
      (k, newV)
    }.toMap
  }
}

def combiner[A : Monoid](x: A, y: A) : A = {
  implicitly[Monoid[A]].combine(x, y)
}

combiner(Map("abc" -> 3, "def" -> 4), Map("def" -> 87))
// res0: scala.collection.immutable.Map[String,Int] = Map(abc -> 3, def -> 91)
```

Here, we merge two maps together, combining the keys without
explicitly having to say that we wanted to add the values together.
The implicit resolution found our AdditiveMonoid instance and used
it.

### Further Reading

[Typelevel Monoid docs](https://typelevel.org/cats/typeclasses/semigroup.html)

---
title: Typeclasses - Part I - Show
date: 2018-11-13 20:51:51
tags: 
- scala
- typeclass
- show
---

If you're coming to Scala from Java, at first it can be pretty easy to think
that Scala is just Java with a slightly more concise Lambda syntax.  This is
because one key tool in the Scala developer's toolkit are typeclasses and at
first they're such a foreign concept that they can be overlooked.  However,
they can be quite powerful at layering functionality without increasing
complexity.

Let's talk about the show typeclass first.  Here's the basic interface for
Show.

``` scala
trait Show[A] {
  def show(a: A) : String
}
```

Show is the typeclass version of having a `toString` method.  This has a lot of
important benefits over the inheritance oriented approach.

1. It gives you a somewhat standard means of providing your own `toString`
   method on a class you do not control.
2. The type system won't allow you to accidentally accept something that doesn't
   have good means of being turned into a String.  No more accidental hashcodes
   finding their way into your output data.
3. The implicits system allows you to build up more complex Show instances.

Let's take a deeper look at #3.  Here's a function we're going to use for
illustration purposes.

``` scala
def printIt[A : Show](a : A) {
  implicitly[Show[A]].show(a)
}
```

This type signature roughly translates to "a function named printIt that takes
an A as input, where A is any type in which a Show[A] is implicitly available in
scope.  This is syntactic sugar for


``` scala
def printIt[A](a : A)(implicit aShow: Show[A]) {
  aShow.show(a)
}
```

Let's try to call this function.

``` scala
printIt(7)
// Error:(29, 75) could not find implicit value for evidence parameter of type A$A11.this.Show[Int]
// def get$$instance$$res0 = /* ###worksheet### generated $$end$$ */ printIt(7)
                                                                         ^
```

That's right, we can't call this without a Show[Int] instance in scope.  Let's
provide that.

``` scala
implicit val showInt = new Show[Int] {
  override def show(a: Int) = a.toString
}

printIt(7)
res0: String = 7
```

So far, there isn't a whole lot of point in this abstraction, but let's look at
how Scala can use implicits to build up increasingly complex typeclass
instances.

These type signatures say "if A is a type that I know how to show (or
have an instance of Show[A]), then I can provide an instance of
Show[Option[A]].  Our Show instance for option has a sometimes desired benefit
of turning None into n/a.

``` scala
implicit def showOption[A : Show] = new Show[Option[A]] {
  override def show(a: Option[A]) = {
    val s = implicitly[Show[A]]
    a.map(v => s.show(v)).getOrElse("n/a")
  }
}

implicit def showList[A: Show]: Show[List[A]] = new Show[List[A]] {
  override def show(a: List[A]): String = {
    val s = implicitly[Show[A]]
    a.map(v => s.show(v)).mkString(" | ")
  }
}
```

Now let's look at what these provide for us.

``` scala
val x : Option[Int] = None
printIt(Option(47))
// res1: String = 47
printIt(x)
// res2: String = n/a
printIt(List(Some(7), None, Some(8)))
//res3: String = 7 | n/a | 8
val z: Option[List[List[Option[Int]]]] =
  Option(List(List(Option(7), None, Option(19)), List(Option(8), None, Option(27))))
printIt(z)
res4: String = 7 | n/a | 19 | 8 | n/a | 27
```

While this is a toy example, the power of Scala typeclasses start to shine a bit
here.  We didn't need to provide anything that knows how to specifically handle
 `Option[List[List[Option[Int]]]`.  The type system basically used the following
 logic.
 1. I need a type that I know how to show, and this type is an Option.  It looks
    like I can show that if and only if I can show what is in the Option.
 2. A List is in the outermost Option.  It looks like I can show that if and
    only if I can show what is in the list.
 3. A List is in the outermost List.  It looks like I can show that if and
    only if I can show what is in the list.
 4. An Option is in the inner List.  It looks like I can show that if and only
    if I can show what is in the Option.
 5. An Int is in the Option, and I know how to show that.  Woohoo, I have
    everything I need to show this!

This is the basis of how tools like Circe are able to provide extremely complex
auto-derived JSON encoders and decoders.

### Further reading

[Typelevel cats Show docs](https://typelevel.org/cats/typeclasses/show.html)

[Typelevel typeclasses](https://typelevel.org/cats/typeclasses.html)

### Some final notes

Libraries like cats and scalaz provide a ton of reasonable show instances for
most scala core primitives and collections.  You won't always need to build your
own instances as I have above.  Usually you would just `import cats.implicits._`
and have all the standard implementations.

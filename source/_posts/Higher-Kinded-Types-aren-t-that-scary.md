---
title: Higher Kinded Types aren't that scary
date: 2018-10-04 20:51:51
tags: 
- scala
- kinds
- hkt
---

Higher Kinded Types (HKTs) seem incredibly intimidating at first but are really
rather simple.  They also have a pretty compelling use case that open up entire
new worlds of code flexibility.  Let's consider the following two pieces of code

```scala
def doIfAllPresent(aOpt: Option[Int], bOpt: Option[Int]) : Option[Int] = {
  for {
    a <- aOpt
    b <- bOpt
  } yield a + b
}
```


```scala
def doifAllComplete(aFut: Future[Int], bFut: Future[Int]) : Future[Int] = {
  for {
    a <- aFut
    b <- bFut
  } yield a + b
}
```

These are pretty clearly the same code with the exception of the type of the
wrapper!  If you come from Java land, you might think "I know, I'll just use
whatever parent class enables for comprehensions!  Unfortunately, Scala's for
comprehensions aren't similar to Java's.  There is no parent class.  In order to
use a for comprehension, you need a thing that has a flatMap method on it, and
unfortunately structural types use runtime reflection and as such they're
absurdly slow.

In fact, there's lots of different classes that share no common type hierarchy with
either Option or Future but would have very similar semantics here.  For
example, Try, List, Set, Task, IO, etc...  Why can't we write one function that
works for all of them?  This is where higher kinded types come in.

We can easily satisfy the type signature we want with the following, but get a
compile time error in the for loop since we can't guarantee that F is a type
that has a flatMap method.

```scala
def doIfAllPresent[F[_]](aF : F[Int], bF: F[Int]) : F[Int] = {
  for {
    a <- aF
    b <- bF
  } yield a + b
}

=> Main.scala:13:12: value flatMap is not a member of type parameter F[Int]
```

`[F[_]]` loosely translates into English as "A type parameter that has one type
hole".  So `List[T]`, `Set[T]`, `Future[T]` would satisfy this constraint.  `Int`,
`String`, and `Either[A, B]` would not.  The first time I understood this, I really
didn't get why this was useful.  You need to know something about the underlying
type right?

While this is sometimes useful all by itself, when combined with typeclasses, it
becomes really powerful.  Let's make a seemingly minor tweak to say that `F` is a type
that we know how to flatMap.

```scala
import cats.Monad
import cats.implicits._
import cats._

def doIfAllPresent[F[_] : Monad](aF : F[Int], bF: F[Int]) : F[Int] = {
  for {
    a <- aF
    b <- bF
  } yield a + b
}

doIfAllPresent(Option(1), Option(2))
 => res0: Option[Int] = Some(3)
doIfAllPresent(List(1, 2), List(3, 4))
 => res1: List[Int] = List(4, 5, 5, 6)
doIfAllPresent(List.empty[Int], List(7, 8))
 => res2: List[Int] = List()
```

So you might ask "you've saved me some copy paste, but that adds some
complexity.  How often is this worth it?"  Like any good tool, the answer is
"sometimes".  The above example is a bit contrived, but as wrapping your network
and other side effects in `IO` becomes increasingly popular, it decreases the
barrier of adoption to not need to make a hard decision on whether users of your
library are using the `IO` from cats-effect, ZIO, monix Tasks, scala core
futures, etc...

Likewise, sometimes it is nice to be able to test code syncronously for unit
tests, but use some kind of Asynchronous contruct when running in production.

So you could do something like this to enable that.

```scala
class SomeClassWithAnInjectedDelegate[F[_] : Monad](delegate : Delegate[F]) : F[Foo] = {
  def foo(i: Int) : F[Int] = delegate.doSomething(i)
}

// In tests
val testClass = new SomeClassWithAnInjectedDelegate[Option](new Delegate[Option] {
  def doSomething(i: Int) = Option(i)
})

// For production
class ConcreteDelegate extends Delegate[Future] {
  def doSomething(i: Int) = Future(i)
}

val myClass = new SomeClassWithInjectedDelegate(new ConcreteDelegate)
```

While you certainly want to write an integration test that is a little closer to
the code you might run in production, this technique can be useful to make the
bulk of your unit tests simpler and more consistent.

### Further Reading

[Higher-kinded types: the difference between giving up, and moving
forward](https://typelevel.org/blog/2016/08/21/hkts-moving-forward.html) - The
fine folks at Typelevel making the same points I'm making here and probably
doing a better job at it.

[http4s](https://http4s.org/v0.19/service/) - Contains a wealth of nice applied
usage of HKTs that enable an extraordinarily flexible API.

[Functional Programming for Mortals](https://leanpub.com/fpmortals) - Explains
HKTs in chapter 1 and is available online as pay what you want.

---
title: Typeclasses - Part III - Improving usability with the "enrich my library" pattern
date: 2018-11-19 20:55:47
tags: 
- scala
- Enrich my library
- typeclass

---

In the last couple posts, we saw how some of the basic typeclasses provided by
libraries like cats and scalaz are implemented and what they can do for us.
However, using them required a ton of boilerplate.  Let's use the "enrich my
library" approach to make usage of these tools require less boilerplate.

```scala
final class SemigroupOps[A: Semigroup](lhs: A) {
  def |+|(rhs: A): A = implicitly[Semigroup[A]].combine(lhs, rhs)
}

implicit def syntaxSemigroup[A: Semigroup](a: A): SemigroupOps[A] =
  new SemigroupOps[A](a)
```

This code is defining a new class with a |+| operator for any type that has a
Semigroup (or means of combining 2 instances of the same type into 1 instance of
that type).  This |+| operator handles the combination.  Here's some example
usage.  

```scala
7 |+| 8
// res0: Int = 15
Map("abc" -> 3, "def" -> 4) |+| Map("def" -> 87)
// res1: scala.collection.immutable.Map[String,Int] = Map(abc -> 3, def -> 91)
```

Now with a simple import, we've added a typesafe |+| operator that we can use to
combine two instances of any class that we have a Semigroup instance in scope
for. In our toy examples in the last post, we only defined semigroup instances
for Int and Map, so this is all we can really illustrate without implementing 
more of them.

So at this point you're probably starting to see how this approach is beneficial
once you write a boatload of boilerplate to abstract away all of these tedious
common operations, but you're probably thinking "Holy shit that's a lot of
boilerplate!" and you would be right to think that.

This approach really isn't all that feasible unless you have a library that has
all of this tedious boilerplate code completed for the majority of types that
you would want to use it with. This is where the cats and scalaz libraries come
in. They handlethe overwhelming majority of this kind of boilerplate so you
don't have to. They provide standard instances of most of these typeclasses for
all of the common types, and have optimized away a fair amount of the rough
edges.


---
title: Init my blag
date: 2018-10-04 20:39:25
tags:
---

Welcome to Arcdrag's blog.  I tend to learn things best as I translate my
learnings into plain English.  As such, this blog will be as much of a learning
tool for me as it is a learning tool for others.  

I plan on mostly talk about Functional Programming, Scala, Haskell, and esoteric
languages here, but realistically, it will be a bit of everything that I've been
working on.  
